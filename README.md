# 2024-07-20
my new superior script https://gitlab.com/sepron/magisklist replaces changetriggerslogic as it does the same thing similarly but a little smarter and alot more dynamically

# changetriggerslogic

shell script to observe files and send them as arguments to some logic script whenever they have new modification times




# example invokation

changetriggerslogic ~/test/5377001441/somelogic ~/test/5377001441/data1 ~/test/5377001441/data2 ~/test/5377001441/data3


{excerpt ~/test/5377001441/somelogic:2
```
printf "%s\n" "$(basename $1)" >> ~/test/5377001441/thatlogfile
```
}

{excerpt ~/test/5377001441/data1:1
```
data1
```
}

{excerpt ~/test/5377001441/data2:1
```
data2
```
}

{excerpt ~/test/5377001441/data3:1
```
data3
```
}



let some process manipulate the modification time of files data1 data3 data2



{excerpt ~/test/5377001441/thatlogfile:1
```
data1
data1
data3
data2
data2
```
}




